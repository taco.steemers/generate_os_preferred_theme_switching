A bash script to generate the css to support automated selection of dark or light website themes.

CSS can be used to select the user's preferred type of theme as set on the operating system or browser level.
Javascript can also be used. These two solutions use a different CSS structure.
What this project does is combine several CSS files in to one CSS file for the CSS solution, 
and several CSS files for the JS solution. 
This is to help avoid duplication of CSS when one is using a [hybrid CSS / JSS implementation of dark theme and light theme support](https://tacosteemers.com/articles/2020-10-17-dark-and-light-web-themes-consider-using-a-hybrid-approach).
This project does not design the CSS for you.

One way to use the script is by calling it as a build step on your project.
The script example/generate_example.sh shows how the scripts can be called.