#!/bin/bash

# https://stackoverflow.com/questions/6393551/what-is-the-meaning-of-0-in-a-bash-script
currentDirectory="${0%/*}"

"$currentDirectory"/../src/generate_css.sh \
"$currentDirectory"/input/lightmode.css \
"$currentDirectory"/input/darkmode.css \
"$currentDirectory"/input/general.css \
"$currentDirectory"/output_css_switching/out.css;

"$currentDirectory"/../src/generate_css_for_js_switching.sh \
"$currentDirectory"/input/general.css \
"$currentDirectory"/input/ \
"$currentDirectory"/output_js_switching/;
